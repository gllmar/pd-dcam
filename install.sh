#!/bin/bash
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

for filename in $SCRIPTPATH/systemd/*.service; do
    [ -e "$filename" ] || continue
	echo "processing $filename"
	sudo cp $filename /etc/systemd/system/
	SERVICE=${filename##*/} # (get only file without basename :: posix compliant :)
	sudo systemctl daemon-reload
	sudo systemctl start $SERVICE
	sudo systemctl enable $SERVICE
done
