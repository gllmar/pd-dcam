#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo ">> LAUNCHING PD USING :: " `which pd`

pd -version

pd -listdev -midiindev "1" -midioutdev "1" -nogui   -open "$DIR/../pd/_main.pd"
